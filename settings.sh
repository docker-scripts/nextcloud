APP=nextcloud
DOMAIN="nc.example.org"

#######################################################################

### Admin settings.
### Comment out to disable automatic installation/configuration.
ADMIN_USER=admin
ADMIN_PASS=123456
ADMIN_EMAIL="admin@example.org"

#######################################################################

### Uncomment DB settings to use a mysql database. Change DBHOST if needed.
### You must have a server running MySQL, for example install 'mariadb'
### container with docker-scripts: https://gitlab.com/docker-scripts/mariadb
#DBHOST=mariadb    # this is the name of the mariadb container
#DBPORT=3306
#DBNAME=nc_example_org
#DBUSER=nc_example_org
#DBPASS=123456

#######################################################################

### Uncomment REDIS_HOST to enable redis configuration. Change hostname if needed.
### You must have a server running Redis to use this feature, for example install
### a Redis container with docker-scripts: https://gitlab.com/docker-scripts/redis
#REDIS_HOST=redis
#REDIS_HOST_PORT=6379
#REDIS_HOST_PASSWORD='ohhebahQuahghaingeef1ifeitah5yei'

#######################################################################

#MAX_UPLOAD=512M
