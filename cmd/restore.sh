cmd_restore_help() {
    cat <<_EOF
    restore <backup-file.tgz> [-d | --nodata]
        Restore from the given backup file.
        Don't restore the data, if the option '-d' or '--nodata' is given.

_EOF
}

cmd_restore() {
    local file=$1
    [[ ! -f $file ]] && fail "Usage:\n$(cmd_restore_help)"
    local backup=${file%%.tgz}
    backup=$(basename $backup)

    # extract the backup archive
    tar --extract --gunzip --preserve-permissions --file=$file

    # switch to maintenance mode
    ds occ maintenance:mode --on

    # restore the content of the database
    if [[ -n $DBHOST ]]; then
        ds mariadb script $(pwd)/$backup/$DBNAME.sql
    fi

    # restore app files
    local exclude=''
    [[ $2 == '-d' || $2 == '--nodata' ]] \
        && exclude='--exclude=data'
    [[ -d $backup/www/data/ ]] \
        || exclude='--exclude=data'
    ds stop
    mkdir -p www-bak
    rsync -a --delete $exclude www/ www-bak
    rsync -a --delete $exclude $backup/www/ www
    ds start

    # enable the site
    ds occ maintenance:mode --off

    # clean up
    rm -rf $backup
}
