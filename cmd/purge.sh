cmd_purge_help() {
    cat <<_EOF
    purge
        Remove the container and erase all the configurations and data.

_EOF
}

cmd_purge() {
    ds stop
    ds remove
    [[ -n $DBHOST ]] && ds mariadb drop
    rm -rf www/ logs/
}
