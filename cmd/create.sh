cmd_create_help() {
    cat <<_EOF
    create
        Create the container '$CONTAINER'.

_EOF
}

rename_function cmd_create orig_cmd_create
cmd_create() {
    mkdir -p www
    orig_cmd_create \
        --mount type=bind,source=$(pwd)/www,destination=/var/www/html \
        "$@"    # accept additional 'docker create' options
}
