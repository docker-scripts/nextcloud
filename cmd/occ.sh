cmd_occ_help() {
    cat <<_EOF
    occ [...]
        Run occ commands inside the container.

_EOF
}

cmd_occ() {
    if test -t 0 ; then
        docker exec -it \
               --user www-data \
               $CONTAINER \
               env TERM=xterm \
               php occ --no-warnings "$@"
    else
        docker exec -i \
               --user www-data \
               $CONTAINER \
               php occ --no-warnings "$@"
    fi
}
