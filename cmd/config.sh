cmd_config_help() {
    cat <<_EOF
    config
        Run configuration scripts inside the container.

_EOF
}

cmd_config() {
    check_settings

    ds inject debian-fixes.sh
    ds inject msmtp.sh
    ds inject logwatch.sh $(hostname)
    ds inject apache2-redirect-to-https.sh
    ds inject setup-apache2.sh
    ds inject setup-cron.sh
    ds inject fix-config.sh

    # create the database
    [[ -n $DBHOST ]] && ds mariadb create

    # install and config
    ds inject update.sh
    nc_config

    # install additional apps, which are listed on apps.txt
    [[ $1 != '--update' ]] && [[ -f apps.txt ]] && ds occ app:enable $(cat apps.txt)
}

check_settings() {
    [[ $DOMAIN == 'nc.example.org' ]] && return 0

    [[ $ADMIN_USER == 'admin' && $ADMIN_PASS == '123456' ]] &&\
        fail "Error: ADMIN_PASS on 'settings.sh' has to be changed for security reasons."
    [[ -n $DBHOST && $DBPASS == '123456' ]] &&\
        fail "Error: DBPASS on 'settings.sh' should be changed for security reasons."
}

nc_config() {
    set -x

    # make some basic configuration
    ds occ user:setting $ADMIN_USER settings email $ADMIN_EMAIL
    set_trusted_domains

    # setup mail settings for sending notifications
    if [[ -n $SMTP_SERVER ]]; then
        setup_smtp_server
    elif [[ -n $GMAIL_ADDRESS ]]; then
        setup_gmail_smtp
    fi

    # setup redis
    setup_redis

    # don't keep trashed files for more than 7 days
    occ_set trashbin_retention_obligation 'auto, 7'
    occ_set versions_retention_obligation 'auto, 7'

    # set maintenance_window_start
    ds occ config:system:set \
       maintenance_window_start --type=integer --value=1

    set +x
}

occ_set() {
    local name=$1 val="$2"
    ds occ config:system:set $name --value="$val"
}

setup_smtp_server() {
    [[ -z $SMTP_SERVER ]] && return

    occ_set mail_smtpmode     'smtp'
    occ_set mail_smtphost     "$SMTP_SERVER"
    occ_set mail_smtpport     '25'
    occ_set mail_smtptimeout  '10'
    occ_set mail_domain       "$SMTP_DOMAIN"
    occ_set mail_from_address 'info'
}

setup_gmail_smtp() {
    [[ -z $GMAIL_ADDRESS ]] && return

    occ_set mail_smtpmode     'smtp'
    occ_set mail_smtphost     'smtp.gmail.com'
    occ_set mail_smtpport     '587'
    occ_set mail_smtptimeout  '10'
    occ_set mail_smtpauthtype 'LOGIN'
    occ_set mail_smtpauth     'true'
    occ_set mail_smtpsecure   'tls'
    occ_set mail_smtpname     "$GMAIL_ADDRESS"
    occ_set mail_smtppassword "$GMAIL_PASSWD"
    occ_set mail_domain       'gmail.com'
    occ_set mail_from_address ${GMAIL_ADDRESS%@*}
}

set_trusted_domains() {
    local trusted_domains="$DOMAIN $DOMAINS $(hostname --all-ip-addresses)"
    local idx=1
    for domain in $trusted_domains; do
        domain=$(echo "$domain" | sed -e 's/^[[:space:]]*//' -e 's/[[:space:]]*$//')
        ds occ config:system:set trusted_domains $idx --value="$domain"
        idx=$((idx + 1))
    done
}

setup_redis() {
    [[ -z $REDIS_HOST ]] && return

    ds occ config:system:set redis host --value="$REDIS_HOST"
    ds occ config:system:set redis port --value="${REDIS_HOST_PORT:-6379}"
    [[ -n $REDIS_HOST_PASSWORD ]] &&\
        ds occ config:system:set redis password --value="$REDIS_HOST_PASSWORD"

    ds occ config:system:set 'memcache.distributed' --value='\OC\Memcache\Redis'
    ds occ config:system:set 'memcache.locking' --value='\OC\Memcache\Redis'
}
