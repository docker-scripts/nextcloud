rename_function cmd_revproxy global_cmd_revproxy
cmd_revproxy() {
    global_cmd_revproxy "$@"
    [[ $1 == 'add' ]] &&  _custom_revproxy_config
} 
  
_custom_revproxy_config() {
    [[ -z $MAX_UPLOAD ]] && return
  
    local config_file=$(ds revproxy path)
    sed -i $config_file \
        -e "/proxy_params/ a \        client_max_body_size $MAX_UPLOAD;"

    # reload the new configuration                                                                                       
    ds @revproxy reload
}
