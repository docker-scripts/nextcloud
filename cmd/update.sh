cmd_update_help() {
    cat <<_EOF
    update
        Update/upgrade to the latest stable version of nextcloud.

_EOF
}

cmd_update() {
    docker pull nextcloud:stable
    ds build
    ds create
    ds config --update
    ds occ app:update --all
}
