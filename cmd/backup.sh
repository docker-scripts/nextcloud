cmd_backup_help() {
    cat <<_EOF
    backup [-d | --nodata]
        Make a backup.
        Don't include the data, if the option '-d' or '--nodata' is given.

_EOF
}

cmd_backup() {
    # create the backup dir
    local backup="backup-$(date +%Y%m%d)"
    rm -rf $backup
    rm -f $backup.tgz
    mkdir $backup

    # switch to maintenance mode
    ds occ maintenance:mode --on

    # dump the content of the database
    if [[ -n $DBHOST ]]; then
        ds mariadb dump > $backup/$DBNAME.sql
    fi

    # copy app files to the backup dir
    local exclude=''
    [[ $1 == '-d' || $1 == '--nodata' ]] \
        && exclude='--exclude=data'
    rsync -a $exclude www/ $backup/www

    # backup settings
    cp settings.sh $backup/
    cp apps.txt $backup/

    # make the backup archive
    tar --create --gzip --preserve-permissions --file=$backup.tgz $backup/
    rm -rf $backup/

    # enable the site
    ds occ maintenance:mode --off
}
