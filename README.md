# NextCloud in a container

https://nextcloud.com/

## Installation

  - First install `ds` and `revproxy`:
    + https://gitlab.com/docker-scripts/ds#installation
    + https://gitlab.com/docker-scripts/revproxy#installation

  - Then get the scripts: `ds pull nextcloud`

  - Create a directory for the container: `ds init nextcloud @nc.example.org`

  - Fix the settings and customize the list of apps to be installed:
    `cd /var/ds/nc.example.org/ ; vim settings.sh; vim apps.txt`

  - Make the container: `ds make`

  - If the domain is not a real one (for example if installed on a local machine),
    add to `/etc/hosts` the line `127.0.0.1 nc.example.org`.
    Then open in browser: https://nc.example.org


## Automatic installation

To make an automatic installation, uncomment `ADMIN_USER` and
`ADMIN_PASS` on `settings.sh`. Make sure to change the default value
of the password.

## Using a MySQL database

To use a mysql database, uncomment the `DBHOST`, `DBPORT`, `DBUSER`
and `DBPASS` on `settings.sh`. Make sure to change the default value
of the password.

`DBHOST` is the hostname or IP or the mysql server. For example you
can install a `mariadb` container with docker-scripts:
https://gitlab.com/docker-scripts/mariadb In this case `DBHOST` is
just the name of the container: `mariadb`.

## Use Redis

To enable Redis, Uncomment `REDIS_HOST` on `settings.sh` and set the
hostname. You must have a server running Redis to use this feature,
for example install a Redis container with docker-scripts:
https://gitlab.com/docker-scripts/redis

## Update nextcloud

To update to the latest stable version of nextcloud simply run: `ds
update`. Usually this is a safe operation and can be run even
automatically from a cron job once in a while.

## Backup and restore

```
ds backup
ds restore <backup-file.tgz>
```

## Run configuration commands

```
ds occ
```

## Other commands

```
ds shell

ds stop
ds start
ds help
```
