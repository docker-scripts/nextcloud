# syntax=docker/dockerfile:1.4

FROM nextcloud:stable

ENV container docker
STOPSIGNAL SIGRTMIN+3

### install systemd
RUN <<EOF
  apt update && apt --yes upgrade
  echo "resolvconf resolvconf/linkify-resolvconf boolean false" | debconf-set-selections
  apt -y install systemd resolvconf
  systemctl set-default multi-user.target
EOF
ENTRYPOINT ["/lib/systemd/systemd"]

### install some other packages
RUN <<EOF
  apt install --yes \
      procps keyboard-configuration locales \
      rsyslog logrotate cron logwatch vim

  # don't try to read the kernel messages inside the container
  sed -i /etc/rsyslog.conf \
      -e '/imklog/s/^/#/'

  apt purge --yes mailutils mailutils-common
  apt autoremove --yes
  apt install --yes msmtp msmtp-mta bsd-mailx
EOF
ENV LANG=C.UTF-8 \
    LC_ALL=C.UTF-8

RUN <<EOF
  ### enable dark background for vim
  sed -i /etc/vim/vimrc \
      -e 's/^"set background=dark/set background=dark/'

  ### customize /root/.bashrc
  cat << '_EOF_' >> /root/.bashrc

# make ls colorized
export LS_OPTIONS='--color=auto'
export SHELL='/bin/bash'
alias ls='ls $LS_OPTIONS'
alias ll='ls $LS_OPTIONS -l'
alias l='ls $LS_OPTIONS -lA'

# set a better prompt
PS1='\n\[\033[01;33m\]\u@\[\033[01;32m\]\h\[\033[00m\]:\[\033[01;34m\]\w\[\e[32m\]\n==> \$ \[\033[00m\]'

_EOF_
EOF

RUN <<EOF
  # enable ssl for apache2
  apt install --yes ssl-cert
  a2enmod ssl
  a2dissite 000-default
  a2ensite default-ssl

  # install php-redis
  rm -f /etc/apt/preferences.d/no-debian-php
  apt update
  apt install --yes php-redis

  # fix the warning "Module php-imagick in this instance has no SVG support."
  apt install --yes libmagickcore-6.q16-6-extra
EOF
