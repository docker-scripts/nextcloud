#!/bin/bash -x

set -o allexport

source /host/settings.sh

NEXTCLOUD_ADMIN_USER=$ADMIN_USER
NEXTCLOUD_ADMIN_PASSWORD=$ADMIN_PASS

if [[ -z $DBHOST ]]; then
    SQLITE_DATABASE=nextcloud
else
    MYSQL_HOST=$DBHOST
    MYSQL_DATABASE=$DBNAME
    MYSQL_USER=$DBUSER
    MYSQL_PASSWORD=$DBPASS
fi

set +o allexport

export NEXTCLOUD_UPDATE=1
/entrypoint.sh su -p www-data -s /bin/sh -c 'php /var/www/html/occ status'

