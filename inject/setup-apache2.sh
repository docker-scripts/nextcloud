#!/bin/bash -x

### fix apache log files
rm -rf /var/log/apache2
mkdir /var/log/apache2
chown www-data: /var/log/apache2

# fix RemoteIPHeader
sed -i /etc/apache2/conf-enabled/remoteip.conf \
    -e '/RemoteIPHeader/ c RemoteIPHeader X-Forwarded-For'

# service discovery for CalDAV or CardDAV
# https://docs.nextcloud.com/server/16/admin_manual/configuration_server/reverse_proxy_configuration.html#apache2
cat <<'EOF' > /etc/apache2/conf-available/service-discovery.conf
RewriteEngine On
RewriteRule ^/\.well-known/carddav https://%{SERVER_NAME}/remote.php/dav/ [R=301,L]
RewriteRule ^/\.well-known/caldav https://%{SERVER_NAME}/remote.php/dav/ [R=301,L]
EOF

# enable HTTP strict transport security
# https://docs.nextcloud.com/server/29/admin_manual/installation/harden_server.html#enable-http-strict-transport-security
cat <<'EOF' > /etc/apache2/conf-available/strict-transport-security.conf
<IfModule mod_headers.c>
    Header always set Strict-Transport-Security "max-age=15552000; includeSubDomains"
</IfModule>
EOF

a2enconf service-discovery
a2enconf strict-transport-security
systemctl reload apache2
