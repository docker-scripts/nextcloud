#!/bin/bash -x

# setup system cron
cat <<EOF > /etc/cron.d/nextcloud
*/10 * * * *    www-data    /usr/local/bin/php -f /var/www/html/cron.php > /dev/null 2>&1
EOF
