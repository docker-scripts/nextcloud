#!/bin/bash -x
### fix php config

### The upstream docker image calls apache2 with 'exec':
### https://github.com/docker-library/php/blob/master/8.0/bullseye/apache/apache2-foreground#L40
### As a result it can access the variables in the environment of root
### However in our image we are installing systemd, which starts
### apache2.  So, it cannot access the env variables PHP_MEMORY_LIMIT,
### PHP_UPLOAD_LIMIT and APACHE_BODY_LIMIT.

source /host/settings.sh

cat <<EOF > /usr/local/etc/php/conf.d/nextcloud.ini
memory_limit=${MAX_UPLOAD:-512M}
upload_max_filesize=${MAX_UPLOAD:-512M}
post_max_size=${MAX_UPLOAD:-512M}
EOF

# see: https://github.com/nextcloud/docker/pull/2065/files
limit=${MAX_UPLOAD:-512M}
limit=${limit/B/}
apache_body_limit=$(numfmt --from=auto $limit)
cat <<EOF > /etc/apache2/conf-available/apache-limits.conf
LimitRequestBody $apache_body_limit
EOF
